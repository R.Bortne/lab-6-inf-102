package INF102.lab6.cheapFlights;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        // Skamfult løst på oppgavegjennomgang 16.11.2023. Kan ikke se en måte å gjøre det stort bedre på.
        WeightedDirectedGraph<City, Integer> g = new WeightedDirectedGraph<>();

        
        for (Flight flight : flights) {
            City start = flight.start;                
            City dest = flight.destination;
            Integer cost = flight.cost;
    
            g.addVertex(start);
                
            g.addVertex(dest);
    
            g.addEdge(start, dest, cost);
        }
    
        return g;
    }

    /**
     * I all hovedsak den samme koden som ble gjennomgått på oppgavegjennomgang 16.11.2023.
     * Noen små justeringer, som f.eks getOrDefault som return for hovedfunksjonen.
     * 
     * Gjorde noen forsøk på å gjøre koden mer lesbar, samt å implementere en Map<City, List<Flight>> for å
     * lagre flyvninger i stede for Trip objekter, men det ble med forsøket. 
     * 
     * Kredit for denne løsningen går derfor til Alexander Höpner. 
     * 
     */
    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);
        Map<City, Integer> prices = dijkstra(graph, start, nMaxStops);
        return prices.getOrDefault(destination, -1);
    }

    private Map<City, Integer> dijkstra(WeightedDirectedGraph<City, Integer> graph, City start, int nMaxStops) {
        Map<ReachInNStops, Trip> bestPrice = new HashMap<>();
        PriorityQueue<Trip> queue = new PriorityQueue<>();
        ReachInNStops startReach = new ReachInNStops(start, 0);
        Trip startTrip = new Trip(startReach, 0);
        addNeighbours(startTrip, graph, queue);
        bestPrice.put(startReach, startTrip);

        while (!queue.isEmpty()) {
            Trip current = queue.remove();
            if (bestPrice.containsKey(current.destInNStops) && 
                bestPrice.get(current.destInNStops).totalPrice <= current.totalPrice) 
                continue;
            bestPrice.put(current.destInNStops, current);
            if (current.destInNStops.nStops <= nMaxStops) {
                addNeighbours(current, graph, queue);
            }
        }

        Map<City, Integer> distances = new HashMap<>();
        for (Trip trip : bestPrice.values()) {
            City city = trip.destInNStops.destination;
            int price = trip.totalPrice;
            if (trip.destInNStops.nStops > nMaxStops + 1) {
                continue;
            }
            distances.putIfAbsent(city, price);
            if (distances.get(city) > price) {
                distances.put(city, price);
            }
        }
        return distances;
    }

    private void addNeighbours(Trip currentTrip, WeightedDirectedGraph<City, Integer> graph, PriorityQueue<Trip> queue) {
        for (City neighbour : graph.outNeighbours(currentTrip.destInNStops.destination)) {
            ReachInNStops reach = new ReachInNStops(neighbour, currentTrip.destInNStops.nStops + 1);
            Trip newTrip = new Trip(reach, currentTrip.totalPrice + graph.getWeight(currentTrip.destInNStops.destination, neighbour));
            queue.add(newTrip);
        }
    }

    class ReachInNStops {
        City destination;
        Integer nStops;

        public ReachInNStops(City destination, Integer nStops) {
            this.destination = destination;
            this.nStops = nStops;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (!(o instanceof ReachInNStops)) return false;
            ReachInNStops other = (ReachInNStops) o;
            return this.destination.equals(other.destination) && this.nStops.equals(other.nStops);
        }

        @Override
        public int hashCode() {
            return Objects.hash(destination, nStops);
        }
    }

    class Trip implements Comparable<Trip> {
        ReachInNStops destInNStops;
        Integer totalPrice;

        public Trip(ReachInNStops reach, Integer price) {
            this.destInNStops = reach;
            this.totalPrice = price;
        }

        @Override
        public int compareTo(Trip o) {
            return Integer.compare(this.totalPrice, o.totalPrice);
        }
    }

}
